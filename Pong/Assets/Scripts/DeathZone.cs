﻿using UnityEngine;

public class DeathZone : MonoBehaviour
{
    private enum DZName
    {
        None, Player1, Player2
    }

    [SerializeField] private DZName _dzName = DZName.None;
    private UIManager _uiManager;

    private void Start()
    {
        _uiManager = FindObjectOfType<UIManager>();
        if (!_uiManager) Debug.Log("UIManager could not be found.");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Ball"))
        {
            Ball ball = collision.GetComponent<Ball>();
            if (ball)
            {
                ball.Die();
                ball.ResetPosition();
            }

            if (_dzName == DZName.Player1) _uiManager.AddPoint(0, 1);
            if (_dzName == DZName.Player2) _uiManager.AddPoint(1, 0);

            _uiManager.ShowStartMessage();
        }
    }
}