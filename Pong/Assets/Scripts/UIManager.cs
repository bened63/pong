﻿using System.Collections;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _pointsTextP1;
    [SerializeField] private TextMeshProUGUI _pointsTextP2;
    [SerializeField] private TextMeshProUGUI _startText;

    private int _pointsP1, _pointsP2;

    private void Start()
    {
        if (!_pointsTextP1) Debug.LogFormat("<color=lime>PointsTextP1 reference is missing.</color>");
        if (!_pointsTextP2) Debug.LogFormat("<color=lime>PointsTextP2 reference is missing.</color>");
        if (!_startText) Debug.LogFormat("<color=lime>StartText reference is missing.</color>");
        ShowStartMessage();
    }

    public void ShowStartMessage()
    {
        StartCoroutine(IEStartTextBlinking());
    }

    public void HideStartMessage()
    {
        StopAllCoroutines();
        _startText.gameObject.SetActive(false);
    }

    private IEnumerator IEStartTextBlinking()
    {
        float wait = 0.5f;
        while (true)
        {
            _startText.gameObject.SetActive(true);
            yield return new WaitForSeconds(wait);
            _startText.gameObject.SetActive(false);
            yield return new WaitForSeconds(wait);
        }
    }

    public void AddPoint(int p1, int p2)
    {
        _pointsP1 += p1;
        _pointsP2 += p2;
        UpdateUI();
    }

    private void UpdateUI()
    {
        _pointsTextP1.text = _pointsP1.ToString();
        _pointsTextP2.text = _pointsP2.ToString();
    }
}