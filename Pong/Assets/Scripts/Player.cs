﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField] private float _speed = 6;
    private float _verticalIput = 0;
    private Ball _ball;
    private UIManager _uiManager;

    private void Start()
    {
        _ball = FindObjectOfType<Ball>();
        if (_ball == null)
        {
            Debug.LogFormat("<color=lime>Ball was not found!</color>");
        }

        _uiManager = FindObjectOfType<UIManager>();
        if (_uiManager == null)
        {
            Debug.LogFormat("<color=lime>UIManager was not found!</color>");
        }
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.W) && transform.position.y <= 4)
        {
            _verticalIput = 1;
        }
        else if (Input.GetKey(KeyCode.S) && transform.position.y >= -4)
        {
            _verticalIput = -1;
        }
        else
        {
            _verticalIput = 0;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            _ball.moving = true;
            _uiManager.HideStartMessage();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }
    }

    private void FixedUpdate()
    {
        transform.Translate(Vector3.up * _verticalIput * Time.deltaTime * _speed);
    }
}