﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Ball _ball;
    private Transform _ballTransform;
    private Vector3 _target;
    [SerializeField] private float _speed = 2;

    private void Start()
    {
        _ball = FindObjectOfType<Ball>();
        if (_ball == null)
        {
            Debug.LogFormat("<color=lime>Ball was not found!</color>");
        }
        _ballTransform = _ball.transform;
    }

    private void FixedUpdate()
    {
        _target = transform.position;
        if (_ballTransform.position.x < 0)
        {
            if (transform.position.y > 0.25f || transform.position.y < -0.25f)
            {
                // Move to center
                _target.y = Random.Range(-0.25f, 0.25f);
            }
        }
        else
        {
            // Move to ball
            _target.y = Mathf.Clamp(_ballTransform.position.y, -4, 4);
        }

        transform.position = Vector3.MoveTowards(transform.position, _target, Time.fixedDeltaTime * _speed);
    }
}