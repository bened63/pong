﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ExampleClass : MonoBehaviour
{
    public Transform originalObject;
    public Transform reflectedObject;
    public Vector3 mirror = new Vector3(1, 0, 0);
    public float _angleReflection;
    [Range(0, 360)] public float _angle;
    public float _angleIn;
    public float _angleOut;
    public float _radius = 1;
    public Vector3 _point;
    public Vector3 _reflection;
    public Vector3 _mirror;

    void Update()
    {
        _mirror = transform.position - transform.right;

        _angleIn = AngleBetweenVector2(transform.position-_point, transform.position-_mirror);

        //_angle = Random.Range(-60, 60);
        float x = transform.position.x + (float)Mathf.Cos(_angle * Mathf.Deg2Rad) * _radius;
        float y = transform.position.y + (float)Mathf.Sin(_angle * Mathf.Deg2Rad) * _radius;
        _point = new Vector3(x, y, 0);


        _angleReflection = -_angle;
        //_angleReflection = -180 - _angle;
        //_angleReflection = 360 + _angleIn; // 180 + 180 - |alpha|
        x = transform.position.x + (float)Mathf.Cos(_angleReflection * Mathf.Deg2Rad) * _radius;
        y = transform.position.y + (float)Mathf.Sin(_angleReflection * Mathf.Deg2Rad) * _radius;
        _reflection = new Vector3(x, y, 0);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, _point);

        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, _reflection);

        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, _mirror);
    }

    /// <summary>
    /// Calculate the angle between two Vector2 vectors.
    /// </summary>
    private float AngleBetweenVector2(Vector2 vec1, Vector2 vec2)
    {
        Vector2 vec1Rotated90 = new Vector2(-vec1.y, vec1.x);
        float sign = (Vector2.Dot(vec1Rotated90, vec2) < 0) ? -1.0f : 1.0f;
        return Vector2.Angle(vec1, vec2) * sign;
    }
}