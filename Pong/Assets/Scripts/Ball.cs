﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    private const float START_SPEED = 4f;

    [SerializeField] private float _acceleration = 0.5f;
    [SerializeField] private AudioClip _blip;
    [SerializeField] private AudioClip _death;

    private float _speed;
    [SerializeField] private float _angle;
    private bool _moving = false;
    private Vector3 _direction;
    private AudioSource _audio;

    public float angle { get => _angle; private set => _angle = value; }
    public bool moving { get => _moving; set => _moving = value; }

    void Start()
    {
        _audio = GetComponent<AudioSource>();
        if (!_audio) Debug.LogFormat("<color=lime>There is no audio source.</color>");
        ResetPosition();
        UpdateDirection();
    }

    void FixedUpdate()
    {
        UpdateDirection();

        if (!_moving) return;
        transform.position = Vector3.MoveTowards(transform.position, _direction, Time.fixedDeltaTime * _speed);
    }

    private void UpdateDirection()
    {
        float x = transform.position.x + (float)Mathf.Cos(_angle * Mathf.Deg2Rad);
        float y = transform.position.y + (float)Mathf.Sin(_angle * Mathf.Deg2Rad);
        _direction = new Vector3(x, y, 0);
    }

    public void IncreaseSpeed()
    {
        _speed = Mathf.Clamp(_speed +_acceleration, 0, 100);
    }

    public void ResetPosition()
    {
        Random.InitState((int)System.DateTime.Now.Ticks);
        int sign = Random.Range(0, 2) == 0 ? -1 : 1;
        int dir = (Random.Range(0, 2) == 0 ? 180 : 0);
        float a = sign * Random.Range(10f, 50f) + dir;
        _angle = a;
        _moving = false;
        _speed = START_SPEED;
        transform.position = Vector3.zero;
    }

    public void Bounce(float angle)
    {
        _angle = angle;
        _audio.clip = _blip;
        _audio.Play();
    }

    public void Die()
    {
        _audio.clip = _death;
        _audio.Play();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, _direction);

        //Gizmos.color = Color.white;
        //Gizmos.DrawLine(transform.position, _reflection);

        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, transform.position + transform.right);
    }
}
