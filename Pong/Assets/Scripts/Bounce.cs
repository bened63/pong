﻿using UnityEngine;

public class Bounce : MonoBehaviour
{
    private enum BounceType
    {
        Wall, Player
    }

    [SerializeField] private BounceType _bounceType = BounceType.Wall;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Ball"))
        {
            Ball ball = collision.GetComponent<Ball>();
            float angle = 0;
            if (_bounceType == BounceType.Wall)
            {
                angle = (_bounceType == BounceType.Wall ? -ball.angle : 180 - ball.angle);
            }
            else if (_bounceType == BounceType.Player)
            {
                Vector3 dir = ball.transform.position - transform.position;
                float a = AngleBetweenVector2(dir, Vector3.right);
                angle = -a;
                ball.IncreaseSpeed();
            }
            ball.Bounce(angle);
        }
    }

    /// <summary>
    /// Calculate the angle between two Vector2 vectors.
    /// </summary>
    private float AngleBetweenVector2(Vector2 vec1, Vector2 vec2)
    {
        Vector2 vec1Rotated90 = new Vector2(-vec1.y, vec1.x);
        float sign = (Vector2.Dot(vec1Rotated90, vec2) < 0) ? -1.0f : 1.0f;
        return Vector2.Angle(vec1, vec2) * sign;
    }
}